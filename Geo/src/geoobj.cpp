#include "../h/geoobj.h"

#include <iostream>

Geo::GeoObj::GeoObj(const Coord& p) 
	: refPoint(p) {
	std::cout << "Creating GeoObj base class." << std::endl;
}

Geo::GeoObj::~GeoObj() {
	std::cout << "Destroying GeoObj base class." << std::endl;
}

int Geo::GeoObj::getGeoX() {
	return refPoint.getX();
}

int Geo::GeoObj::getGeoY() {
	return refPoint.getY();
}

void Geo::GeoObj::move(const Coord& offset) {
	refPoint.addTo(offset);
}