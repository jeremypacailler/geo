#include "../h/coord.h"

#include <iostream>

Geo::Coord::Coord(int x, int y) {
    std::cout << "Calling coord constructor." << std::endl;
    this->x = x;
    this->y = y; 
}

int Geo::Coord::getX() {
    return this->x;
}

int Geo::Coord::getY() {
    return this->y;
}

Geo::Coord Geo::Coord::negate() {
    return (Geo::Coord(-(this->x), -(this->y)));
}

void Geo::Coord::addTo(const Geo::Coord &choord) {
    this->x += choord.x;
    this->y += choord.y;
}
