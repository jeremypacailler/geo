#include "../h/rectangle.h"

#include <iostream>

Geo::Rectangle::Rectangle(const Geo::Coord& coord, 
	int length, int width) : GeoObj(coord)
{
	this->length = length;
	this->width = width;
}

Geo::Rectangle::~Rectangle()
{
}

int Geo::Rectangle::getLength()
{
	return this->length;
}

int Geo::Rectangle::getWidth()
{
	return this->width;
}

void Geo::Rectangle::setLength(int length)
{
	this->length = length;
}

void Geo::Rectangle::setWidth(int width)
{
	this->width = width;
}

int Geo::Rectangle::getArea()
{
	return length * width;
}

int Geo::Rectangle::getPerimeter()
{
	return ((length + width)*2);
}

void Geo::Rectangle::draw()
{
	std::cout
		<< "Drawing a rectangle with upper left hand corner point ("
		<< this->refPoint.getX() << ","
		<< this->refPoint.getY()
		<< ") with length " << this->length
		<< " and width " << this->width << std::endl;
}

void Geo::Rectangle::displayInfo()
{
	std::cout
		<< "Rectangle Information:" << std::endl
		<< "\t\t Reference point: ("
				<< refPoint.getX() << "," << refPoint.getY()
				<< ")" << std::endl
		<< "\t\t Length: " << this->getLength() << std::endl
		<< "\t\t Width: " << this->getWidth() << std::endl
		<< "\t\t Area: " << this->getArea() << std::endl
		<< "\t\t Perimeter: " << this->getPerimeter() << std::endl
		<< std::endl;
}
