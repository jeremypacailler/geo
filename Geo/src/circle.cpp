#include "../h/circle.h"

#include <iostream>

Geo::Circle::Circle(const Geo::Coord& p, int r) 
    : GeoObj(p), radius(r)
{
    std::cout << "Creating circle." << std::endl;
}

Geo::Circle::~Circle()
{
    std::cout << "Destroying circle." << std::endl;
}

double Geo::Circle::getArea()
{
    return PI * this->radius * this->radius;
}

double Geo::Circle::getCircumference()
{
    return 2 * PI * this->radius;
}

void Geo::Circle::draw()
{
    std::cout
        << "Drawing a circle around center point ("
            << this->refPoint.getX() << ","
            << this->refPoint.getY()
        << ") with radius " << radius << std::endl;
}

void Geo::Circle::displayInfo()
{
    std::cout
        << "Circle Information:" << std::endl
        << "\t\t Center point: ("
                << refPoint.getX() << "," << refPoint.getY()
                << ")" << std::endl
        << "\t\t Radius: " << this->radius << std::endl
        << "\t\t Area: " << this->getArea() << std::endl
        << "\t\t Circumference: " << this->getCircumference()
        << std::endl << std::endl;
}
