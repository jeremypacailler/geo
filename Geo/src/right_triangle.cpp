#include "../h/right_triangle.h"

#include <iostream>

Geo::Right_Triangle::Right_Triangle(const Geo::Coord& coord,
	int height, int base) 
	: GeoObj(coord), height(height), base(base)
{
	hypotenuse = sqrt(height * height + base * base);
}

Geo::Right_Triangle::~Right_Triangle()
{
}

int Geo::Right_Triangle::getHeight()
{
	return this->height;
}

int Geo::Right_Triangle::getBase()
{
	return this->base;
}

double Geo::Right_Triangle::getHypotenuse()
{
	// to avoid compiler warnings, we cast smaller into bigger
	return sqrt(height * height + base * base);
}

void Geo::Right_Triangle::setHeight(int height)
{
	this->height = height;
}

void Geo::Right_Triangle::setBase(int base)
{
	this->base = base;
}

double Geo::Right_Triangle::getArea()
{
	return (.5 * base * height);
}

double Geo::Right_Triangle::getPerimeter()
{
	// to avoid compiler warnings, we cast smaller into bigger
	return ((double)height + 
			(double)base + 
			this->getHypotenuse());
}

void Geo::Right_Triangle::draw()
{
	std::cout
		<< "Drawing a right triangle with upper left hand corner point ("
		<< this->refPoint.getX() << ","
		<< this->refPoint.getY()
		<< ") with height " << height << ", base " << base
		<< ", and hypotenuse " << getHypotenuse() << std::endl;
}

void Geo::Right_Triangle::displayInfo()
{
	std::cout
		<< "Right Triangle Information:" << std::endl
		<< "\t\t Reference point: ("
				<< refPoint.getX() << "," << refPoint.getY()
				<< ")" << std::endl
		<< "\t\t Base: " << this->base << std::endl
		<< "\t\t Height: " << this->height << std::endl
		<< "\t\t Hypotenuse: " << this->getHypotenuse() << std::endl
		<< "\t\t Area: " << this->getArea() << std::endl
		<< "\t\t Perimeter: " << this->getPerimeter()
		<< std::endl << std::endl;
}
