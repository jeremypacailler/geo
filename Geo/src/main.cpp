// header file for I/O
#include <iostream>

// header files for used classes
#include "../h/circle.h"
#include "../h/rectangle.h"
#include "../h/right_triangle.h"
#include "../h/geogroup.h"

int main()
{
    Geo::Circle c(Geo::Coord(3, 3), 11);
    Geo::Rectangle r(Geo::Coord(10, 20), 70, 35);
    Geo::Right_Triangle t(Geo::Coord(5, 10), 3, 4);
    Geo::GeoGroup g(Geo::Coord(5, 5));

    g.add(c);					// Add to GeoGroup - circle c
    g.add(r);					//				   - rectangle r
    g.add(t);					//				   - right triangle t    

    std::cout << "Displaying object information before drawing." << std::endl;
    std::cout << "---------------------------------------------" << std::endl;

    g.displayInfo();			// display information about all shapes

    std::cout << "Drawing objects relative to group reference point located at position (" << g.getGeoX() << "," << g.getGeoY() << ")." << std::endl;
    std::cout << "------------------------------------------------------------------------------" << std::endl;
    g.draw();					// draw GeoGroup
    std::cout << std::endl;

    g.move(Geo::Coord(3, -3));  // move offset of GeoGroup
    std::cout << "Redrawing objects after moving group reference point by (3,-3)." << std::endl;
    std::cout << "Group reference point now located at position (" << g.getGeoX() << "," << g.getGeoY() << ")." << std::endl;
    std::cout << "---------------------------------------------------------------" << std::endl;
    g.draw();                  // draw GeoGroup again
    std::cout << std::endl;

    g.remove(c);			   // GeoGroup now only contains r and t
    std::cout << "Redrawing objects after removing the circle." << std::endl;
    std::cout << "------------------------------------------------" << std::endl;
    g.draw();                  // draw GeoGroup again

    system("pause");
    return 0;
}
