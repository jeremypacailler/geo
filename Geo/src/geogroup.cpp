#include "../h/geogroup.h"

Geo::GeoGroup::GeoGroup(Coord coord) : GeoObj(coord)
{
}

Geo::GeoGroup::~GeoGroup()
{
}

void Geo::GeoGroup::draw()
{
	for (unsigned i = 0; i < shapes.size(); ++i) {
		shapes[i]->move(refPoint); // add offset for the reference point
		shapes[i]->draw(); // draw element
		shapes[i]->move(refPoint.negate()); // subtract offset
	}
}

void Geo::GeoGroup::displayInfo()
{
	for(int i = 0; i < shapes.size(); i++) {
		shapes.at(i)->displayInfo();
		std::string name = "hello";
	}	
}

void Geo::GeoGroup::add(GeoObj& obj)
{
	shapes.push_back(&obj);
}

bool Geo::GeoGroup::remove(GeoObj& obj)
{
	// find first element with this address and remove it
	// return whether an object was found and removed
	std::vector<GeoObj*>::iterator pos;
	pos = std::find(shapes.begin(), shapes.end(), &obj);
	if (pos != shapes.end()) {
		shapes.erase(pos);
		return true;
	}
	else
		return false;
}

