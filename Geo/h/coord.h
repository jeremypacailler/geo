#ifndef COORD_H
#define COORD_H

namespace Geo {
    class Coord {
    protected:
        int x;
        int y;

    public:
        Coord(int x = 0, int y = 0);
        int getX();
        int getY();
        Coord negate();
        void addTo(const Coord& coord);
    };
}

#endif // COORD_H