#ifndef GEOOBJ_H
#define GEOOBJ_H

#include "coord.h"

namespace Geo {
	class GeoObj {
	protected:
		Coord refPoint;

	public:
		GeoObj(const Coord& p = Coord(0, 0));
		virtual ~GeoObj();

		int getGeoX();
		int getGeoY();
		void move(const Coord& offset);
		virtual void draw() = 0;
		virtual void displayInfo() = 0;
	};
}

#endif // GEOOBJ_H