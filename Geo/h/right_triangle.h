#ifndef RIGHT_TRIANGLE_H
#define RIGHT_TRIANGLE_H

#include "geoobj.h"
#include <math.h>

namespace Geo {
	class Right_Triangle : public GeoObj {
	private:
		int height;
		int base;
		double hypotenuse;

	public:
		Right_Triangle(const Geo::Coord& coord,
			int height, int base);
		~Right_Triangle();
		int getHeight();
		int getBase();
		double getHypotenuse();
		void setHeight(int height);
		void setBase(int base);
		double getArea();
		double getPerimeter();
		void draw() override;
		void displayInfo() override;
	};
}

#endif // RIGHT_TRIANGLE_H