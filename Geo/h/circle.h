#ifndef CIRCLE_H
#define CIRCLE_H

#include "geoobj.h"

const double PI = 3.14;

namespace Geo {
	class Circle : public GeoObj {
	private:
		int radius;
	public:
		Circle(const Geo::Coord& p, int r);
		~Circle();
		double getArea();
		double getCircumference();
		void draw() override;
		void displayInfo() override;
	};
}

#endif // CIRCLE_H
