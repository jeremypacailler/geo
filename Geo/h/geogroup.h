#ifndef GEOGROUP_H
#define GEOGROUP_H

#include "geoobj.h"
#include <vector>

namespace Geo {
	class GeoGroup : public GeoObj {
	private:
		std::vector<GeoObj*> shapes;
	
	public:
		GeoGroup(Coord coord);
		~GeoGroup();
		void draw() override;
		void displayInfo() override;
		void add(GeoObj& obj);
		bool remove(GeoObj& obj);
	};
}

#endif // GEOGROUP_H