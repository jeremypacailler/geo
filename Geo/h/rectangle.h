#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "coord.h"
#include "geoobj.h"

namespace Geo {
	class Rectangle : public GeoObj {
	private:
		int length;
		int width;

	public:
		Rectangle(const Geo::Coord& coord, int length, int width);
		~Rectangle();
		int getLength();
		int getWidth();
		void setLength(int length);
		void setWidth(int width);
		int getArea();
		int getPerimeter();
		void draw() override;
		void displayInfo() override;
	};
}

#endif // RECTANGLE_H